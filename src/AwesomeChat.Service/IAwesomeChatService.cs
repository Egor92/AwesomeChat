﻿using System;
using System.ServiceModel;
using AwesomeChat.Common;

namespace AwesomeChat.Service
{
    [ServiceContract(CallbackContract = typeof(IAwesomeChatServiceCallback))]
    public interface IAwesomeChatService
    {
        [OperationContract]
        Result<Guid> Connect();

        [OperationContract]
        Result Disconnect(Guid clientId);

        [OperationContract]
        Result<User> Login(Guid clientId, string userName);

        [OperationContract]
        Result Logout(User user);

        [OperationContract]
        void SendMessage(User user, string message);
    }
}
