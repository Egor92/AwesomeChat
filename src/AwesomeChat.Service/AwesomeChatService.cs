﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using AwesomeChat.Common;

namespace AwesomeChat.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single, ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class AwesomeChatService : IAwesomeChatService
    {
        private readonly Dictionary<Guid, IAwesomeChatServiceCallback> _clients = new Dictionary<Guid, IAwesomeChatServiceCallback>();
        private readonly IList<User> _loggedUsers = new List<User>();

        public Result<Guid> Connect()
        {
            var callback = OperationContext.Current.GetCallbackChannel<IAwesomeChatServiceCallback>();
            if (callback == null)
                return Result.Fault<Guid>("Client's callback wasn't found");

            Guid clientId = Guid.NewGuid();
            _clients[clientId] = callback;

            using (new UsingConsoleColor(ConsoleColor.Green))
            {
                Console.Write("Client ");
                using (new UsingConsoleColor(ConsoleColor.DarkYellow))
                {
                    Console.Write(clientId);
                }
                Console.WriteLine(" connected");
            }

            return Result.Success(clientId);
        }

        public Result Disconnect(Guid clientId)
        {
            using (new UsingConsoleColor(ConsoleColor.Red))
            {
                Console.Write("Client ");
                using (new UsingConsoleColor(ConsoleColor.DarkYellow))
                {
                    Console.Write(clientId);
                }
                Console.WriteLine(" disconnected");
            }

            _clients.Remove(clientId);
            return Result.Success();
        }

        public Result<User> Login(Guid clientId, string userName)
        {
            userName = userName.Trim();
            if (_loggedUsers.Any(x => x.Name == userName))
                return Result.Fault<User>($"User with name '{userName}' have already logged in");

            var user = new User()
            {
                Name = userName,
                ClientId = clientId,
            };
            _loggedUsers.Add(user);

            using (new UsingConsoleColor(ConsoleColor.Green))
            {
                Console.Write("User ");
                using (new UsingConsoleColor(ConsoleColor.Yellow))
                {
                    Console.Write(user.Name);
                }
                Console.WriteLine(" joined the chat");
            }

            return Result.Success(user);
        }

        public Result Logout(User user)
        {
            _loggedUsers.Remove(user);

            using (new UsingConsoleColor(ConsoleColor.Red))
            {
                Console.Write("User ");
                using (new UsingConsoleColor(ConsoleColor.Yellow))
                {
                    Console.Write(user.Name);
                }
                Console.WriteLine(" left the chat");
            }

            return Result.Success();
        }

        public void SendMessage(User user, string message)
        {
            Console.Write("User ");
            using (new UsingConsoleColor(ConsoleColor.Yellow))
            {
                Console.Write(user.Name);
            }
            Console.WriteLine(" wrote:");

            using (new UsingConsoleColor(ConsoleColor.White))
            {
                Console.WriteLine(message);
            }

            List<Guid> disconnectedClientIds = new List<Guid>();

            foreach (var pair in _clients)
            {
                var clientId = pair.Key;
                var callback = pair.Value;

                try
                {
                    callback.SendMessageToClients(user, message);
                }
                catch (Exception e)
                {
                    disconnectedClientIds.Add(clientId);
                    using (new UsingConsoleColor(ConsoleColor.Red, ConsoleColor.Gray))
                    {
                        Console.Write("Exception when trying to send message to clients");
                        Console.Write(e);
                    }
                }
            }

            foreach (Guid clientId in disconnectedClientIds)
            {
                _clients.Remove(clientId);
            }
        }
    }
}
