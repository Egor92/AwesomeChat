﻿using System.ServiceModel;
using AwesomeChat.Common;

namespace AwesomeChat.Service
{
    public interface IAwesomeChatServiceCallback
    {
        [OperationContract]
        void SendMessageToClients(User sender, string message);
    }
}