﻿using System;
using System.ServiceModel;
using AwesomeChat.Common;
using AwesomeChat.ConsoleClient.ChatService;

namespace AwesomeChat.ConsoleClient
{
    class Program
    {
        private const string Title = "AwesomeChat console client";

        static void Main()
        {
            Console.Title = Title;
            User user = null;

            try
            {
                var callback = new AwesomeChatServiceCallback();
                callback.MessageReceived.Subscribe(message =>
                {
                    if (user == null)
                        return;

                    Console.Write("User ");
                    using (new UsingConsoleColor(ConsoleColor.Yellow))
                    {
                        Console.Write(message.Sender.Name);
                    }
                    Console.WriteLine(" wrote:");

                    using (new UsingConsoleColor(ConsoleColor.White))
                    {
                        Console.WriteLine(message.Text);
                    }
                });

                var instanceContext = new InstanceContext(callback);
                using (var awesomeChatServiceClient = new AwesomeChatServiceClient(instanceContext, "NetTcpBinding_IAwesomeChatService"))
                {
                    var clientId = ConnectToServer(awesomeChatServiceClient);
                    if (clientId != null)
                    {
                        user = Login(awesomeChatServiceClient, clientId.Value);
                        Console.Title = $"{Title}, [{user.Name}]";

                        Talk(awesomeChatServiceClient, user);
                        Logout(awesomeChatServiceClient, user);
                        DisconnectFromServer(awesomeChatServiceClient, clientId.Value);
                    }
                }
            }
            catch (Exception e)
            {
                using (new UsingConsoleColor(ConsoleColor.Red, ConsoleColor.Gray))
                {
                    Console.WriteLine("Unhandled error occurred");
                    Console.WriteLine(e);
                }
            }

            Console.WriteLine("Press <ENTER>");
            Console.ReadLine();
        }

        private static Guid? ConnectToServer(IAwesomeChatService awesomeChatServiceClient)
        {
            var openSessionResult = awesomeChatServiceClient.Connect();
            if (!openSessionResult.IsSuccess)
            {
                using (new UsingConsoleColor(ConsoleColor.Red))
                {
                    Console.WriteLine("Can not connect to server!");
                    Console.WriteLine(openSessionResult.Message);
                    return null;
                }
            }

            var clientId = openSessionResult.Data;
            using (new UsingConsoleColor(ConsoleColor.Green))
            {
                Console.Write("Connected successfully with id ");
                using (new UsingConsoleColor(ConsoleColor.DarkYellow))
                {
                    Console.WriteLine(clientId);
                }
            }

            return clientId;
        }

        private static User Login(IAwesomeChatService client, Guid clientId)
        {
            while (true)
            {
                string userName = GetUserName();

                var loginResult = client.Login(clientId, userName);
                if (!loginResult.IsSuccess)
                {
                    using (new UsingConsoleColor(ConsoleColor.Red))
                    {
                        Console.WriteLine(loginResult.Message);
                    }
                    continue;
                }

                return loginResult.Data;
            }
        }

        private static string GetUserName()
        {
            while (true)
            {
                Console.Write("Enter your name: ");

                string userName;
                using (new UsingConsoleColor(ConsoleColor.Yellow))
                {
                    userName = Console.ReadLine();
                    Console.WriteLine();
                }

                if (!string.IsNullOrWhiteSpace(userName))
                    return userName.Trim();

                using (new UsingConsoleColor(ConsoleColor.DarkRed))
                {
                    Console.WriteLine("Name is empty!");
                }
            }
        }

        private static void Talk(IAwesomeChatService awesomeChatServiceClient, User user)
        {
            while (true)
            {
                string message;
                using (new UsingConsoleColor(ConsoleColor.Green))
                {
                    message = Console.ReadLine();
                }

                if (message != null && message.ToUpper() == "Q")
                    break;

                awesomeChatServiceClient.SendMessage(user, message);
            }
        }

        private static void Logout(IAwesomeChatService awesomeChatServiceClient, User user)
        {
            awesomeChatServiceClient.Logout(user);
        }

        private static void DisconnectFromServer(IAwesomeChatService awesomeChatServiceClient, Guid clientId)
        {
            var disconnectResult = awesomeChatServiceClient.Disconnect(clientId);
            if (!disconnectResult.IsSuccess)
            {
                using (new UsingConsoleColor(ConsoleColor.Red))
                {
                    Console.WriteLine("Can not disconnect from server!");
                    Console.WriteLine(disconnectResult.Message);
                    return;
                }
            }

            using (new UsingConsoleColor(ConsoleColor.Red))
            {
                Console.Write("Disconnected successfully with id ");
                using (new UsingConsoleColor(ConsoleColor.DarkYellow))
                {
                    Console.WriteLine(clientId);
                }
            }
        }
    }
}
