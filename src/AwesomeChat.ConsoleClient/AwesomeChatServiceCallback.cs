using System;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using AwesomeChat.Common;
using AwesomeChat.ConsoleClient.ChatService;

namespace AwesomeChat.ConsoleClient
{
    [DebuggerDisplay("Sender: {" + nameof(Sender) + "}; Text: {" + nameof(Text) + "}")]
    public class Message
    {
        public Message(User sender, string text)
        {
            Sender = sender;
            Text = text;
        }

        public User Sender { get; set; }

        public string Text { get; set; }
    }

    public class AwesomeChatServiceCallback : IAwesomeChatServiceCallback
    {
        #region MessageReceived

        private readonly ISubject<Message> _messageReceived = new Subject<Message>();

        public IObservable<Message> MessageReceived
        {
            get { return _messageReceived.AsObservable(); }
        }

        #endregion

        public void SendMessageToClients(User sender, string text)
        {
            var message = new Message(sender, text);
            _messageReceived.OnNext(message);
        }
    }
}
