﻿using System.Diagnostics;
using System.Runtime.Serialization;

namespace AwesomeChat.Common
{
    [DataContract]
    [DebuggerDisplay("IsSuccess: {" + nameof(IsSuccess) + "}; Message: {" + nameof(Message) + "}")]
    public class Result
    {
        protected Result(bool isSuccess, string message = null)
        {
            IsSuccess = isSuccess;
            Message = message;
        }

        [DataMember(Order = 0)]
        public bool IsSuccess { get; set; }

        [DataMember(Order = 1)]
        public string Message { get; set; }

        public static Result Success()
        {
            return new Result(true);
        }

        public static Result<T> Success<T>(T data)
        {
            return new Result<T>(true)
            {
                Data = data,
            };
        }

        public static Result Fault(string message)
        {
            return new Result(false, message);
        }

        public static Result<T> Fault<T>(string message)
        {
            return new Result<T>(false, message);
        }
    }

    [DataContract]
    [DebuggerDisplay("IsSuccess: {" + nameof(IsSuccess) + "}; Message: {" + nameof(Message) + "}; Data: {" + nameof(Data) + "}")]
    public sealed class Result<T> : Result
    {
        internal Result(bool isSuccess, string message = null)
            : base(isSuccess, message)
        {
        }

        [DataMember(Order = 2)]
        public T Data { get; set; }
    }
}
