﻿using System;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace AwesomeChat.Common
{
    [DataContract]
    [DebuggerDisplay("Name: {" + nameof(Name) + "}; ClientId: {" + nameof(ClientId) + "}")]
    public class User
    {
        [DataMember(Order = 0)]
        public string Name { get; set; }

        [DataMember(Order = 1)]
        public Guid ClientId { get; set; }
    }
}
