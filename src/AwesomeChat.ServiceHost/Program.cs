﻿using System;
using AwesomeChat.Common;
using AwesomeChat.Service;

namespace AwesomeChat.ServiceHost
{
    class Program
    {
        static void Main()
        {
            Console.Title = "AwesomeChat.ServiceHost";
            try
            {
                using (var serviceHost = new System.ServiceModel.ServiceHost(typeof(AwesomeChatService)))
                {
                    serviceHost.Open();

                    using (new UsingConsoleColor(ConsoleColor.Green))
                    {
                        Console.WriteLine("Service started");
                    }
                    using (new UsingConsoleColor(ConsoleColor.Yellow))
                    {
                        foreach (var address in serviceHost.BaseAddresses)
                        {
                            Console.WriteLine("- address: {0}", address);
                        }
                    }
                    Console.WriteLine();
                    Console.WriteLine("Press <ENTER> to close service");
                    Console.ReadLine();
                }
            }
            catch (Exception e)
            {
                using (new UsingConsoleColor(ConsoleColor.Red))
                {
                    Console.WriteLine(e.ToString());
                }
                Console.WriteLine("Press <ENTER> to close the application");
                Console.ReadLine();
            }
        }
    }
}
